#include "global.h"

struct winsize ws;

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

void clear(){
    printf("\e[1;1H\e[2J");
}

void printLifes(int a, int l) {
    int f = (ws.ws_col / 2 - (a * 3) / 2);
    int j, o, b = 0;
    for (o = 0; o < 3; o++) {
        for (j = 0; j < ws.ws_col; j++) {
            if (o == 1 && (j + ((a % 2) == 0 ? 1 : 0)) % 3 == 0 && j > f && j <= f + a * 3) {
                b++;
                printf(b <= l ? "+" : "-");
            } else {
                printf("=");
            }
        }
        printf("\n");
    }
}

void printWords(char * words) {
    printf("Felhasznált betűk: ");
    int i;
    for(i = 0; i < strlen(words); i++)
        printf("%s%c", i > 0 ? ", " : "", words[i]);
    printf("\n");
}

void printTips(char * tips) {
    int len = strlen(tips);
    int start = ws.ws_col / 2 - len + (len % 2 == 0 ? 0 : 1);
    int i;
    for(i = 0; i < start; i++)
        printf(" ");
    for(i = 0; i < len; i++)
        printf("%c ", tips[i]);
    printf("\n");
}

void updateScreen(int maxLife, int now, char * usedWords, char * tip){
    clear();
    printLifes(maxLife, now);
    printf("\n");
    printTips(tip);
    printf("\n");
    printWords(usedWords);
    printf("\n");
}

int main(){
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);

    int on = 1;

    char buffer[32];

    struct sockaddr_in server;

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(PORT);

    if (connect(fd, (struct sockaddr *)&server, sizeof server) < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    while(1){
        recv(fd, buffer, 32, 0);
        if (buffer[0] == '\0') break;
        
        if (valid(buffer, ":get") == 0) {
            printf("Írd be a kitalálandó szót: ");

            fgets(buffer, sizeof buffer, stdin);
            *strchr(buffer, '\n') = '\0';

            send(fd, ":setword", 32, 0);
            send(fd, buffer, strlen(buffer), 0);
        } else if (valid(buffer, ":tip") == 0) {
            printf("Tippelj meg egy betűt: ");
            fgets(buffer, sizeof buffer, stdin);
            *strchr(buffer, '\n') = '\0';
            send(fd, ":tip", 32, 0);
            send(fd, buffer, strlen(buffer), 0);
        } else if (valid(buffer, ":update") == 0) {
            char maxLifeC[4];
            recv(fd, maxLifeC, 4, 0);
            int maxLife = strtol(maxLifeC, NULL, 10);

            char nowC[4];
            recv(fd, nowC, 4, 0);
            int now = strtol(nowC, NULL, 10);

            char usedWords[50];
            recv(fd, usedWords, 50, 0);

            char tips[50];
            recv(fd, tips, 50, 0);

            updateScreen(maxLife, now, usedWords, tips);
        } else if (valid(buffer, ":waitForWord") == 0) {
            printf("Az ellenfeled éppen megad egy kitalálandó szót.\n");
        } else if (valid(buffer, ":wait") == 0) {
            printf("Várakozás a másik játékosra...\n");
        } else if (valid(buffer, ":exists") == 0) {
            printf("Erre a betűre már tippeltél...\n");
        } else if (valid(buffer, ":alpha") == 0) {
            printf("Csak ASCII betűket adj meg...\n");
        } else if (valid(buffer, ":winner") == 0) {
            printf("Kitalálták a megfelelő szót!\nMivel te nyertél te írod be a következő kitalálandó szót.\n");
        } else if (valid(buffer, ":win") == 0) {
            printf("Kitaláltad a megfelelő szót!\n");
        } else if (valid(buffer, ":surr") == 0) {
            printf("Feladtad a játékot!\nAz ellenfeled új szót talál ki neked!\n");
        } else if (valid(buffer, ":ff") == 0) {
            printf("Az ellenfeled feladta a játékot!\nTalálj ki neki egy könnyebb szót!\n");
        } else if (valid(buffer, ":lose") == 0) {
            printf("Sajnos nem sikerült kitalálnod a megfelelő szót!\nEzért az ellenfeled kitalál neked egy másikat!\n");
        } else if (valid(buffer, ":nofound") == 0) {
            printf("Az ellenfeled nem találta ki a megfelelő szót!\nÍgy kitalálhatsz neki egy másikat.\n");
        } else if (valid(buffer, ":exit") == 0) {
            printf("A játék véget ért.\n");
        } else {
            printf("Fogadva: %s\n", buffer);
        }

        bzero(buffer, sizeof(buffer));
    }
    printf("Kilep!\n");

    close(fd);

    return 0;
}