#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include "server.h"

struct thread_args {
    int *client;
    struct sockaddr_in *addr;
    int *id;
};

int sockets[MAX];
int fd;
int ships[MAX][10][10];
int placed[MAX];

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

void closeHandler(int dummy) {
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if(sockets[i] > -1)
            close(sockets[i]);
    if(fd && fd > -1)
        shutdown(fd, SHUT_RDWR);
    printf("A szerver bezárva!\n");
    exit(1);
}

int main(int argc , char *argv[]) {
	int fd_client, c, *new_sock, on = 1, max = MAX, i, id, *new_id;
	struct sockaddr_in server, client, *new_addr;
	
    for(i = 0; i < max; i++) {
        sockets[i] = -1;
        placed[i] = 0;
    }

    signal(SIGINT, closeHandler);

	fd = socket(AF_INET , SOCK_STREAM , 0);
	if (fd == -1){
		printf("create fail");
        exit(1);
	}
	
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	
	if( bind(fd,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("bind fail");
		return 1;
	}

	listen(fd , 10);
	
	printf("Kapcsolatok varasa...\n");
	c = sizeof(struct sockaddr_in);

	while((fd_client = accept(fd, (struct sockaddr *)&client, (socklen_t*)&c)) )
	{
        id = 0;
        while(sockets[id] != -1 && id < max)
            id++;
        if (id >= max) {
            printf("Kapcsolat elutasitva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
            close(fd_client);
            continue;
        }

        sockets[id] = fd_client;

		printf("Kapcsolat fogadva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
		
        struct thread_args args;
		pthread_t sniffer_thread;
		new_sock = (int*)malloc(sizeof(int));
		*new_sock = fd_client;
        new_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
        *new_addr = client;
        new_id = (int*)malloc(sizeof(int));
        *new_id = id;

        args.client = new_sock;
        args.addr = new_addr;
        args.id = new_id;

		if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*)&args) < 0)
		{
			perror("could not create thread");
			return 1;
		}
	}
	
	if (fd_client < 0)
	{
		perror("accept failed");
		return 1;
	}

    close(fd);
	
	return 0;
}

void sendTo(int sock, char * msg, int len){
    char newt[len];
    int i = 0;
    do {
        newt[i] = msg[i];
    } while(msg[i++]);
    send(sock, newt, len, 0);
}

int countShips(int p) {
    int i, j, c = 0;
    for (i = 0; i < 10; i++)
        for (j = 0; j < 10; j++)
            if(ships[p][i][j] == 1)
                c++;
    return c;
}

void *connection_handler(void *arguments)
{
    int read_size, i;
    struct thread_args *args = (struct thread_args *) arguments;
	int sock = *(int*)args->client;
    int id = *(int*)args->id;
    struct sockaddr_in addr = *(args->addr);
	char client_message[32], wordS[1024], oneS[4];

    if (id == 0) {
        sendTo(sock, ":waitforother", 32);
        printf("Fellépett egy genyo\n");
        for(i = 0; i < MAX; i++)
            placed[i] = 0;
    } else {
        sendTo(sockets[0], ":get", 32);
        sendTo(sock, ":get", 32);
    }

	while ((read_size = recv(sock, client_message, 32, 0)) > 0) {
        if (valid(client_message, ":sendwords") == 0) {
            bzero(wordS, sizeof(wordS));
            recv(sock, wordS, 1024, 0);

            printf("%s\n", wordS);
   
        } else if (valid(client_message, ":placeship") == 0) {
            bzero(oneS, sizeof(oneS));
            recv(sock, oneS, 4, 0);
            int x = atoi(oneS);
            recv(sock, oneS, 4, 0);
            int y = atoi(oneS);

            ships[id][x][y] = 1;
            placed[id]++;

            if (placed[0] >= 5 && placed[1] >= 5) {
                sendTo(sockets[0], ":rocket", 32);
                sendTo(sockets[1], ":waitforrocket", 32);
            } else {
                if (placed[id] >= 5) {
                    sendTo(sock, ":placedall", 32);
                } else {
                    sendTo(sock, ":get", 32);
                }
            }

            printf("Új hajó lerakva: %d - %d\n", x, y);


        } else if (valid(client_message, ":sendrocket") == 0) {
            recv(sock, oneS, 4, 0);
            int x = atoi(oneS);
            recv(sock, oneS, 4, 0);
            int y = atoi(oneS);

            printf("Torpedó %d - %d\n", x, y);

            if (ships[id == 0 ? 1 : 0][x][y] == 1) {
                ships[id == 0 ? 1 : 0][x][y] = 3;

                sendTo(sock, ":boom", 32);
                snprintf(oneS, sizeof(oneS), "%d", 1);
                sendTo(sock, oneS, 4);

                sendTo(sockets[id == 0 ? 1 : 0], ":boom", 32);
                snprintf(oneS, sizeof(oneS), "%d", 0);
                sendTo(sockets[id == 0 ? 1 : 0], oneS, 4);

            } else {
                ships[id == 0 ? 1 : 0][x][y] = 2;

                sendTo(sock, ":fail", 32);
                snprintf(oneS, sizeof(oneS), "%d", 1);
                sendTo(sock, oneS, 4);

                sendTo(sockets[id == 0 ? 1 : 0], ":fail", 32);
                snprintf(oneS, sizeof(oneS), "%d", 0);
                sendTo(sockets[id == 0 ? 1 : 0], oneS, 4);
            }

            snprintf(oneS, sizeof(oneS), "%d", x);
            sendTo(sock, oneS, 4);
            sendTo(sockets[id == 0 ? 1 : 0], oneS, 4);

            snprintf(oneS, sizeof(oneS), "%d", y);
            sendTo(sock, oneS, 4);
            sendTo(sockets[id == 0 ? 1 : 0], oneS, 4);


            if (countShips(0) == 0) {
                sendTo(sockets[1], ":win", 32);
                sendTo(sockets[0], ":lose", 32);
                for(i = 0; i < MAX; i++)
                    if (sockets[i] > -1)
                        close(sockets[i]);
                close(fd);
                exit(1);
            } else if (countShips(1) == 0) {
                sendTo(sockets[0], ":win", 32);
                sendTo(sockets[1], ":lose", 32);
                for(i = 0; i < MAX; i++)
                    if (sockets[i] > -1)
                        close(sockets[i]);
                close(fd);
                exit(1);
            } else {
                sendTo(sockets[id == 0 ? 1 : 0], ":rocket", 32);
                sendTo(sock, ":waitforrocket", 32);
            }

        } else {
		    write(sock, client_message, read_size); //Ismeretlen üzenet
        }
		bzero(client_message, sizeof(client_message));
	}
	
	if (read_size == 0) {
        printf("Kapcsolat bezarult: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        sockets[id] = -1;
	} else if(read_size == -1) {
		perror("recv failed");
	}

	return 0;
}