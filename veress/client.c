#include "global.h"

char lang[1];

int ships[MAX][10][10];
int enabledOther = 0;

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

void clear(){
    printf("\e[1;1H\e[2J");
}

void printShips(int ot) {
    int i, j, c;
    if (ot == 1) {
        printf("      A te hajóid            Az ellenfeled hajói\n\n");
        printf("   0 1 2 3 4 5 6 7 8 9       0 1 2 3 4 5 6 7 8 9\n");
    } else {
        printf("      A te hajóid\n\n");
        printf("   0 1 2 3 4 5 6 7 8 9\n");
    }
    for (i = 0; i < 10; i++) {
        printf("%d  ", i);
        c = 0;
        for (j = 0; j < (ot == 1 ? 20 : 10); j++) {
            if (j > 0 && j % 10 == 0){
                printf("    %d ", i);
                c = 1;
            }
            if (ships[c][i][j % 10] == 1)
                printf("\x1B[32m■\x1B[0m ");
            else if (ships[c][i][j % 10] == 3)
                printf("\x1B[33m▧\x1B[0m ");
            else if (ships[c][i][j % 10] == 2)
                printf("\x1B[31m▣\x1B[0m ");
            else
                printf("□ ");
        }
        printf("\n");
    }
}

void update() {
    clear();
    printShips(enabledOther);
}

int main() {
    int on = 1;

    char buffer[32], pS[4];

    struct sockaddr_in server;

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(PORT);

    if (connect(fd, (struct sockaddr *)&server, sizeof server) < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    while(1){
        recv(fd, buffer, 32, 0);
        if (buffer[0] == '\0') break;
        
        if (valid(buffer, ":get") == 0 || valid(buffer, ":rocket") == 0) {
            if (valid(buffer, ":rocket") == 0)
                enabledOther = 1;

            update();

            while(1) {
                if (valid(buffer, ":get") == 0)
                    printf("Rakd le a hajóidat (X Y): ");
                else
                    printf("Hova szeretnéd küldeni a torpedót (X Y): ");
                
                int x, y;
                scanf("%d %d", &x, &y);

                if (x < 0 || x > 10) {
                    printf("Az x koordináta hibás!\n");
                    continue;
                } else if (y < 0 || y > 10) {
                    printf("Az y koordináta hibás!\n");
                    continue;
                }
                
                if (valid(buffer, ":get") == 0) {
                    if(ships[0][x][y] > 0){
                        printf("Itt már található egy hajó!\n");
                        continue;
                    }
                } else {
                    if(ships[1][x][y] > 0){
                        printf("Ezt a pozíciót már használtad!\n");
                        continue;
                    }
                }

                if (valid(buffer, ":get") == 0)
                    ships[0][x][y] = 1;
                send(fd, valid(buffer, ":get") == 0 ? ":placeship" : ":sendrocket", 32, 0);
                snprintf(pS, sizeof(pS), "%d", x);
                send(fd, pS, 4, 0);
                snprintf(pS, sizeof(pS), "%d", y);
                send(fd, pS, 4, 0);

                update();
                break;
            }
        } else if (valid(buffer, ":boom") == 0 || valid(buffer, ":fail") == 0) {
            recv(fd, pS, 4, 0);
            int c = atoi(pS);

            recv(fd, pS, 4, 0);
            int x = atoi(pS);

            recv(fd, pS, 4, 0);
            int y = atoi(pS);

            ships[c][x][y] = valid(buffer, ":boom") == 0 ? 2 : 3;

            update();

            if (valid(buffer, ":boom") == 0) {
                if (c == 0) {
                    printf("Egy torpedó eltalálta a hajódat a %d:%d pontban.\n", x, y);
                } else {
                    printf("Eltaláltál egy hajót %d:%d pontban.\n", x, y);
                }
            } else {
                if (c == 0) {
                    printf("Egy torpedó becsapódott a %d:%d pontban.\n", x, y);
                } else {
                    printf("Nem találtál el senkit a %d:%d pontban.\n", x, y);
                }
            }

        } else if (valid(buffer, ":waitforrocket") == 0) {
            enabledOther = 1;
            update();
            printf("A másik fél éppen kilő egy torpedót...\n");
        } else if (valid(buffer, ":waitforother") == 0) {
            printf("Várakozás a másik fél érkezésére...\n");
        } else if (valid(buffer, ":placedall") == 0) {
            printf("Leraktad az összes hajódat, várd meg amíg az ellenfeled is végez...\n");
        } else if (valid(buffer, ":win") == 0) {
            printf("Gratulálunk! Megnyerted a játékot.\n");
        } else if (valid(buffer, ":lose") == 0) {
            printf("Sajnos vesztettél.\n");
        } else if (valid(buffer, ":update") == 0) {
            update();
        } else {
            printf("Fogadva: %s\n", buffer);
        }

        bzero(buffer, sizeof(buffer));
    }
    printf("Kilep!\n");

    close(fd);

    return 0;
}
