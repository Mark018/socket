#include "global.h"

char lang[1];

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

int isUpper(char * txt) {
    int i;
    for (i = 0; i < strlen(txt); i++)
        if (txt[i] >= 'a' && txt[i] <= 'z')
            return 0;
    return 1;
}

int isLower(char * txt) {
    int i;
    for (i = 0; i < strlen(txt); i++)
        if (txt[i] >= 'A' && txt[i] <= 'Z')
            return 0;
    return 1;
}

int main(){
    int on = 1;

    char buffer[32], text[1024], shorttext[255];

    struct sockaddr_in server;

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(PORT);

    if (connect(fd, (struct sockaddr *)&server, sizeof server) < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    while(1){
        recv(fd, buffer, 32, 0);
        if (buffer[0] == '\0') break;
        
        if (valid(buffer, ":getlang") == 0) {
            printf("Válaszd ki a nyelved (1 = magyar | 2 = MAGYAR): ");
            fgets(buffer, sizeof buffer, stdin);
            *strchr(buffer, '\n') = '\0';
            send(fd, ":setlang", 32, 0);
            send(fd, buffer, 1, 0);
        } else if (valid(buffer, ":vote") == 0) {
            printf("Szavazz! Egyetértesz? (1 = igen | 2 = nem | 3 = tartózkodom): ");
            fgets(buffer, sizeof buffer, stdin);
            *strchr(buffer, '\n') = '\0';
            send(fd, ":setvote", 32, 0);
            send(fd, buffer, 1, 0);
        } else if (valid(buffer, ":get") == 0) {
            bzero(buffer, sizeof(buffer));
            recv(fd, buffer, 32, 0);
            int bSize = atoi(buffer);

            while(1) {
                printf("Üzenet: ");
                bzero(buffer, sizeof(buffer));
                fgets(buffer, sizeof buffer, stdin);
                *strchr(buffer, '\n') = '\0';

                if(atoi(lang) == 2){
                    if (isUpper(buffer) == 0){
                        printf("A nyelved szerint nagy betűvel kell írnod!\n");
                        continue;
                    }
                } else if(atoi(lang) == 1){
                    if (isLower(buffer) == 0){
                        printf("A nyelved szerint kis betűvel kell írnod!\n");
                        continue;
                    }
                }

                send(fd, ":sendwords", 32, 0);
                send(fd, buffer, bSize, 0);
                break;
            }
        } else if (valid(buffer, ":lang") == 0) {
            recv(fd, lang, 1, 0);
            printf("A nyelved átállítottad: %s\n", lang);
        } else if (valid(buffer, ":ready") == 0) {
            printf("Mindketten kiválasztottátok a nyelvet...\n");
        } else if (valid(buffer, ":waitforother") == 0) {
            printf("Várakozás a másik fél érkezésére...\n");
        } else if (valid(buffer, ":waittovote") == 0) {
            printf("Várakozás a másik fél szavazatára...\n");
        } else if (valid(buffer, ":anw") == 0) {
            printf("Most te kérdezel...\n");
        } else if (valid(buffer, ":text") == 0) {
            bzero(text, sizeof(text));
            recv(fd, text, 1024, 0);
            printf("%s\n", text);
        } else if (valid(buffer, ":shorttext") == 0) {
            bzero(shorttext, sizeof(shorttext));
            recv(fd, shorttext, 255, 0);
            printf("%s\n", shorttext);
        } else if (valid(buffer, ":denied") == 0) {
            printf("Sajnos nem értetek egyet!\n");
        } else if (valid(buffer, ":end") == 0) {
            printf("Mindketten elfogadtátok!\n");
        } else {
            printf("Fogadva: %s\n", buffer);
        }

        bzero(buffer, sizeof(buffer));
    }
    printf("Kilep!\n");

    close(fd);

    return 0;
}
