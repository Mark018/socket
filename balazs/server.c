#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include "server.h"

struct thread_args {
    int *client;
    struct sockaddr_in *addr;
    int *id;
};

int sockets[MAX];
int socketLang[MAX];
int socketVote[MAX];
int waction = 0;
int fd;

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

void closeHandler(int dummy) {
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if(sockets[i] > -1)
            close(sockets[i]);
    if(fd && fd > -1)
        shutdown(fd, SHUT_RDWR);
    printf("A szerver bezárva!\n");
    exit(1);
}

int main(int argc , char *argv[]) {
	int fd_client, c, *new_sock, on = 1, max = MAX, i, id, *new_id;
	struct sockaddr_in server, client, *new_addr;
	
    for(i = 0; i < max; i++){
        sockets[i] = -1;
        socketLang[i] = -1;
        socketVote[i] = -1;
    }

    signal(SIGINT, closeHandler);

	fd = socket(AF_INET , SOCK_STREAM , 0);
	if (fd == -1){
		printf("create fail");
        exit(1);
	}
	
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	
	if( bind(fd, (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("bind fail");
		return 1;
	}

	listen(fd, 10);
	
	printf("Kapcsolatok varasa...\n");
	c = sizeof(struct sockaddr_in);

	while((fd_client = accept(fd, (struct sockaddr *)&client, (socklen_t*)&c)) )
	{
        id = 0;
        while(sockets[id] != -1 && id < max)
            id++;
        if (id >= max) {
            printf("Kapcsolat elutasitva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
            close(fd_client);
            continue;
        }

        sockets[id] = fd_client;

		printf("Kapcsolat fogadva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
		
        struct thread_args args;
		pthread_t sniffer_thread;
		new_sock = (int*)malloc(sizeof(int));
		*new_sock = fd_client;
        new_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
        *new_addr = client;
        new_id = (int*)malloc(sizeof(int));
        *new_id = id;

        args.client = new_sock;
        args.addr = new_addr;
        args.id = new_id;

		if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*)&args) < 0)
		{
			perror("could not create thread");
			return 1;
		}
	}
	
	if (fd_client < 0)
	{
		perror("accept failed");
		return 1;
	}

    close(fd);
	
	return 0;
}

void sendTo(int sock, char * msg, int len){
    char newt[len];
    int i = 0;
    do {
        newt[i] = msg[i];
    } while(msg[i++]);
    send(sock, newt, len, 0);
}

char * toLowerCase(char * txt) {
    int i;
    for (i = 0; i < strlen(txt); i++)
        if(txt[i] != ' ')
            txt[i] = tolower(txt[i]);
    return txt;
}

char * toUpperCase(char * txt) {
    int i;
    for (i = 0; i < strlen(txt); i++)
        if(txt[i] != ' ')
            txt[i] = toupper(txt[i]);
    return txt;
}

void *connection_handler(void *arguments)
{
    int read_size, i;
    struct thread_args *args = (struct thread_args *) arguments;
	int sock = *(int*)args->client;
    int id = *(int*)args->id;
    struct sockaddr_in addr = *(args->addr);
	char client_message[32], wordS[1024], oneS[1];

    if (id == 0) {
        sendTo(sock, ":waitforother", 32);
        printf("Fellépett egy genyo\n");
        waction = 0;
    } else {
        sendTo(sockets[0], ":getlang", 32);
        sendTo(sock, ":getlang", 32);
        printf("Nyelv bekérése...\n");
    }

	while ((read_size = recv(sock, client_message, 32, 0)) > 0) {
        if (valid(client_message, ":sendwords") == 0){
            bzero(wordS, sizeof(wordS));
            recv(sock, wordS, 1024, 0);

            printf("%s\n", wordS);
            if (id == waction) {
                sendTo(sockets[waction == 0 ? 1 : 0], ":text", 32);

                if (socketLang[waction == 0 ? 1 : 0] == 2)
                    sendTo(sockets[waction == 0 ? 1 : 0], toUpperCase(wordS), 1024);
                else
                    sendTo(sockets[waction == 0 ? 1 : 0], toLowerCase(wordS), 1024);
                
                sendTo(sockets[waction == 0 ? 1 : 0], ":get", 32);
                sendTo(sockets[waction == 0 ? 1 : 0], "255", 32);
            } else {
                sendTo(sockets[0], ":shorttext", 32);

                if (socketLang[0] == 2)
                    sendTo(sockets[0], toUpperCase(wordS), 255);
                else
                    sendTo(sockets[0], toLowerCase(wordS), 255);

                if (waction > 0) {
                    sendTo(sockets[0], ":vote", 32);
                    sendTo(sockets[1], ":vote", 32);
                } else {
                    sendTo(sockets[1], ":anw", 32);
                    sendTo(sockets[1], ":get", 32);
                    sendTo(sockets[1], "1024", 32);
                }
                waction = 1;
            }

        } else if (valid(client_message, ":setlang") == 0){
            bzero(oneS, sizeof(oneS));
            recv(sock, oneS, 1, 0);
            int langI = atoi(oneS);

            printf("A nyelv átállítva: %d\n", langI);

            sendTo(sock, ":lang", 32);
            sendTo(sock, oneS, 1);

            socketLang[id] = langI;

            if(socketLang[0] > -1 && socketLang[1] > -1){
                printf("Mindketten beállították a nyelvet!\n");

                sendTo(sockets[0], ":ready", 32);
                sendTo(sockets[1], ":ready", 32);
                sendTo(sockets[0], ":get", 32);
                sendTo(sockets[0], "1024", 32);
            }
        } else if (valid(client_message, ":setvote") == 0){
            bzero(oneS, sizeof(oneS));
            recv(sock, oneS, 1, 0);
            int voteI = atoi(oneS);

            printf("Szavazat érkezett: %d\n", voteI);
            
            socketVote[id] = voteI;

            if(socketVote[0] > -1 && socketVote[1] > -1){
                printf("Mindketten szavaztak!\n");

                if (socketVote[0] == 1 && socketVote[1] == 1) {
                    sendTo(sockets[0], ":end", 32);
                    sendTo(sockets[1], ":end", 32);
                    for(i = 0; i < MAX; i++)
                        close(sockets[i]);
                    close(fd);
                    exit(1);
                } else {
                    sendTo(sockets[0], ":denied", 32);
                    sendTo(sockets[1], ":denied", 32);
                    sendTo(sockets[0], ":get", 32);
                    sendTo(sockets[0], "1024", 32);

                    waction = 0;

                    for(i = 0; i < MAX; i++)
                        socketVote[i] = -1;
                }
            } else {
                sendTo(sock, ":waittovote", 32);
            }
        } else {
		    write(sock, client_message, read_size); //Ismeretlen üzenet
        }
		bzero(client_message, sizeof(client_message));
	}
	
	if (read_size == 0) {
        printf("Kapcsolat bezarult: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        sockets[id] = -1;
	} else if(read_size == -1) {
		perror("recv failed");
	}

	return 0;
}