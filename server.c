#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include "server.h"

struct thread_args {
    int *client;
    struct sockaddr_in *addr;
    int *id;
};

int sockets[MAX];
int fd;

char * word;
char * tip;
int lifeLeft;
char placeholder[50];

int master;

int valid(char msg[], char * to){
    int i = 0;
    for(i = 0; i < strlen(to); i++)
        if(msg[i] != to[i])
            return 1;
    return 0;
}

void closeHandler(int dummy) {
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if(sockets[i] > -1)
            close(sockets[i]);
    if(fd && fd > -1)
        shutdown(fd, SHUT_RDWR);
    printf("A szerver bezárva!\n");
    exit(1);
}

int main(int argc , char *argv[]) {
	int fd_client, c, *new_sock, on = 1, max = MAX, i, id, *new_id;
	struct sockaddr_in server, client, *new_addr;
	
    word = (char*) malloc(32);
    tip = (char*) malloc(32);

    for(i = 0; i < max; i++)
        sockets[i] = -1;

    signal(SIGINT, closeHandler);

	fd = socket(AF_INET , SOCK_STREAM , 0);
	if (fd == -1){
		printf("create fail");
        exit(1);
	}
	
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	
	if( bind(fd,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("bind fail");
		return 1;
	}

	listen(fd , 10);
	
	printf("Kapcsolatok varasa...\n");
	c = sizeof(struct sockaddr_in);

	while((fd_client = accept(fd, (struct sockaddr *)&client, (socklen_t*)&c)) )
	{
        id = 0;
        while(sockets[id] != -1 && id < max)
            id++;
        if (id >= max) {
            printf("Kapcsolat elutasitva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
            close(fd_client);
            continue;
        }

        sockets[id] = fd_client;

		printf("Kapcsolat fogadva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
		
        struct thread_args args;
		pthread_t sniffer_thread;
		new_sock = (int*)malloc(sizeof(int));
		*new_sock = fd_client;
        new_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
        *new_addr = client;
        new_id = (int*)malloc(sizeof(int));
        *new_id = id;

        args.client = new_sock;
        args.addr = new_addr;
        args.id = new_id;

		if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*)&args) < 0)
		{
			perror("could not create thread");
			return 1;
		}
	}
	
	if (fd_client < 0)
	{
		perror("accept failed");
		return 1;
	}

    close(fd);
	
	return 0;
}

void writeTo(int sock, char * msg){
    write(sock, msg, strlen(msg));
}

void sendTo(int sock, char * msg, int len){
    char newt[len];
    int i = 0;
    do {
        newt[i] = msg[i];
    } while(msg[i++]);
    send(sock, newt, len, 0);
}

void sendWithoutMaster(char * msg, int len) {
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if (sockets[i] > -1 && sockets[i] != master)
            sendTo(sockets[i], msg, len);
}

void sendToMaster(char * msg, int len) {
    sendTo(master, msg, len);
}

int inArray(char * msg, char to) {
    int i;
    for(i = 0; i < strlen(msg); i++)
        if(msg[i] == to)
            return 1;
    return 0;
}

void update(int sock) {
    sendTo(sock, ":update", 32);
    char chr[4];
    snprintf(chr, sizeof(chr), "%d", LIFE);
    sendTo(sock, chr, 4);
    snprintf(chr, sizeof(chr), "%d", lifeLeft);
    sendTo(sock, chr, 4);
    sendTo(sock, tip, 50);
    bzero(placeholder, sizeof(placeholder));
    int i;
    for(i = 0; i < strlen(word); i++)
        if(inArray(tip, *(word + i)))
            placeholder[i] = *(word + i);
        else
            placeholder[i] = '_';
    sendTo(sock, placeholder, 50);
}

void updateAll() {
    printf("Képernyő frissítés küldése mindenkinek\n");
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if(sockets[i] > -1)
            update(sockets[i]);
}

int isAlphabet(char c){
    return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) ? 0 : 1;
}

int isEqual(char * str1, char * str2) {
    int i;
    for(i = 0; i < strlen(str1); i++)
        if(isAlphabet(str1[i]) == 0 && str1[i] != str2[i])
            return 1;
    return 0;
}

void win(int sock){
    sendWithoutMaster(":win", 32);
    sendToMaster(":winner", 32);

    master = sock;
    printf("A master át lett állítva -> %d\n", master);

    sendWithoutMaster(":waitForWord", 32);
    sendTo(sock, ":get", 32);
}

void *connection_handler(void *arguments)
{
    int read_size, i;
    struct thread_args *args = (struct thread_args *) arguments;
	int sock = *(int*)args->client;
    int id = *(int*)args->id;
    struct sockaddr_in addr = *(args->addr);
	char client_message[32], wordS[32], tipS[32];

    if(id == 0){
        if (sockets[1] == -1)
            sendTo(sock, ":wait", 32);
        else
            update(sock);
    } else {
        master = sockets[0];
        sendWithoutMaster(":waitForWord", 32);
        sendTo(sockets[0], ":get", 32);
        printf("A master át lett állítva -> %d\n", master);
    }

	while ((read_size = recv(sock, client_message, 32, 0)) > 0) {
        if (valid(client_message, ":setword") == 0){
            bzero(wordS, sizeof(wordS));
            recv(sock, wordS, 32, 0);

            for(i = 0; i < strlen(wordS); i++)
                if(isAlphabet(wordS[i]) == 1) {
                    sendTo(sock, ":alpha", 32);
                    sendTo(sock, ":get", 32);
                    continue;
                }

            tip = (char *) malloc(32);
            word = wordS;
            lifeLeft = LIFE;

            updateAll();

            sendWithoutMaster(":tip", 32);
            printf("A szó be lett állítva: %s\n", word);
        } else if (valid(client_message, ":tip") == 0){
            bzero(tipS, sizeof(tipS));
            recv(sock, tipS, 32, 0);
            printf("Új tipp érkezett: %s\n", tipS);
            if (isAlphabet(tipS[0]) == 0) {
                if(isEqual("felad", tipS) == 0){
                    sendWithoutMaster(":surr", 32);
                    sendToMaster(":ff", 32);
                    sendTo(master, ":get", 32);
                    continue;
                }
                if(isEqual("vege", tipS) == 0){
                    sendWithoutMaster(":exit", 32);
                    sendToMaster(":exit", 32);
                    for(i = 0; i < MAX; i++)
                        if(sockets[i] > -1)
                            close(sockets[i]);
                    exit(1);
                    continue;
                }
                if (isEqual(word, tipS) == 0) {
                    tip = word;
                    updateAll();
                    win(sock);
                    continue;
                } else {
                    if (inArray(tip, tipS[0]) == 0) {
                        char * ch = (char *)malloc(1);
                        ch[0] = tipS[0];
                        strcat(tip, ch);

                        if (inArray(word, tipS[0]) == 0)
                            lifeLeft--;

                        updateAll();

                        if (lifeLeft > 0) {
                            int n = 0;
                            for (i = 0; i < strlen(word); i++)
                                if (inArray(tip, *(word + i)) || *(word + i) == ' ')
                                    n++;

                            if (n == strlen(word)) {
                                win(sock);
                                continue;
                            }
                        } else {
                            sendWithoutMaster(":lose", 32);
                            sendToMaster(":nofound", 32);
                            sendTo(master, ":get", 32);
                            continue;
                        }
                    } else {
                        sendTo(sock, ":exists", 32);
                    }
                }
            } else {
                sendTo(sock, ":alpha", 32);
            }

            sendTo(sock, ":tip", 32);
        } else {
		    write(sock, client_message, read_size); //Ismeretlen üzenet
        }
		bzero(client_message, sizeof(client_message));
	}
	
	if (read_size == 0) {
        printf("Kapcsolat bezarult: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        sockets[id] = -1;
	} else if(read_size == -1) {
		perror("recv failed");
	}

	return 0;
}