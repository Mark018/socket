#include "global.h"

int myCards[5];

int main(){
    int on = 1, i, re;

    char buffer[32], card[4], buffer2[32], buffer3[512];

    struct sockaddr_in server;

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(PORT);

    if (connect(fd, (struct sockaddr *)&server, sizeof server) < 0) {
        printf("Hiba a letrehozaskor!\n");
        exit(1);
    }

    while(1){
        recv(fd, buffer, 32, 0);
        if (buffer[0] == '\0') break;
        
        if (valid(buffer, ":getcard") == 0) {
            for (i = 0; i < 5; i++) {
                bzero(card, sizeof(card));
                recv(fd, card, 4, 0);
                myCards[i] = atoi(card);
            }
        } else if (valid(buffer, ":sent") == 0) {
            printf("\n\nA kezedben: ");
            for (i = 0; i < 5; i++) {
                printf("%d: %s%s", i, numberToCard(myCards[i]), i == 4 ? "" : ", ");
            }
        } else if (valid(buffer, ":startrep") == 0) {
            printf("\nHány kártyát szeretnél cserélni (-1 = feladás): ");
            scanf("%d", &re);

            if (re == -1) {
                printf("Feladtad a játékot!\n");
                send(fd, ":surrender", 32, 0);
                break;
            }

            bzero(card, sizeof(card));
            snprintf(card, sizeof(card), "%d", re);
            send(fd, ":replace", 32, 0);
            send(fd, card, 4, 0);
        } else if (valid(buffer, ":getrep") == 0) {
            bzero(card, sizeof(card));
            recv(fd, card, 4, 0);
            printf("%s. csere: ", card);
            scanf("%d", &re);
            send(fd, ":setcard", 32, 0);
            bzero(card, sizeof(card));
            snprintf(card, sizeof(card), "%d", re);
            send(fd, card, 4, 0);
        } else if (valid(buffer, ":setnew") == 0) {
            bzero(card, sizeof(card));
            recv(fd, card, 4, 0);
            myCards[re] = atoi(card);
        } else if (valid(buffer, ":done") == 0) {
            printf("A te lapjaid: \n");
            for (i = 0; i < 5; i++) {
                printf("%s%s", numberToCard(myCards[i]), (i == 4 ? "\n" : ", "));
            }
            printf("Várakozás az ellenfélre...\n");
        } else if (valid(buffer, ":win") == 0) {
            recv(fd, buffer, 32, 0);
            recv(fd, buffer2, 32, 0);
            bzero(buffer3, sizeof(buffer3));
            recv(fd, buffer3, 512, 0);

            printf("Megnyerted a játékot! Neked '%s' volt, az ellenfelednek '%s'.\nEllenfeled kártyái: %s\n", buffer, buffer2, buffer3);
        } else if (valid(buffer, ":lose") == 0) {
            recv(fd, buffer, 32, 0);
            recv(fd, buffer2, 32, 0);
            bzero(buffer3, sizeof(buffer3));
            recv(fd, buffer3, 512, 0);

            printf("Sajnos vesztettél! Neked '%s' volt, az ellenfelednek '%s'.\nEllenfeled kártyái: %s\n", buffer, buffer2, buffer3);
        } else if (valid(buffer, ":draft") == 0) {
            recv(fd, buffer, 32, 0);
            bzero(buffer3, sizeof(buffer3));
            recv(fd, buffer3, 512, 0);

            printf("Döntetlen! Mind kettőtöknek '%s' volt.\nEllenfeled kártyái: %s\n", buffer, buffer3);
        } else if (valid(buffer, ":end") == 0) {
            printf("A játék véget ért.\n");
        } else if (valid(buffer, ":surr") == 0) {
            printf("Az ellenfeled feladta a játékot.\n");
        } else if (valid(buffer, ":waitforother") == 0) {
            printf("Várakozás a másik játékos érkezésére.\n");
        } else {
            printf("Fogadva: %s\n", buffer);
        }

        bzero(buffer, sizeof(buffer));
    }
    printf("Kilep!\n");

    close(fd);

    return 0;
}
