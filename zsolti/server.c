#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include <math.h>
#include "server.h"

struct thread_args {
    int *client;
    struct sockaddr_in *addr;
    int *id;
};

int sockets[MAX];
int fd;
int games;
int now;

int usedCards[52];
int playerCards[MAX][5];
int playerReps[MAX];
int playerRepDones[MAX];

int getRandomCard() {
    int card, i, j;
    while (1) {
        card = rand() % 52;
        if (usedCards[card] == 1)
            continue;
        usedCards[card] = 1;
        break;
    }
    return card;
}

void giveCardsToClient() {
    int i, j;
    for (i = 0; i < MAX; i++)
        for (j = 0; j < 5; j++)
            playerCards[i][j] = getRandomCard();
}

int isFlush(int pl) {
    int i;
    int matchColor = floor(playerCards[pl][0] / 13);
    for (i = 1; i < 5; i++)
        if (matchColor != floor(playerCards[pl][i] / 13))
            return 0;
    return 1;
}

int isHighCard(int card) {
    return card % 13 >= 8 ? 1 : 0;
}

int isOneHighCard(int pl) {
    int i;
    for (i = 1; i < 5; i++)
        if(isHighCard(playerCards[pl][i]) == 1)
            return 1;
    return 0;
}

int isRoyalFlush(int pl) {
    int i;
    if(isFlush(pl) == 1) {
        for (i = 1; i < 5; i++)
            if(isHighCard(playerCards[pl][i]) == 0)
                return 0;
    } else {
        return 0;
    }
    return 1;
}



int isStraight(int pl) {
    int list[5];
    int i, j;
    for (i = 0; i < 5; i++) {
        int card = playerCards[pl][i] % 13;
        list[i] = card;
    }

    //Bubble sort
    for (i = 0; i < 5 - 1; i++)
        for(j = 0; j < 5 - i - 1; j++)
            if (list[j] > list[j + 1])
                swap(&list[j], &list[j + 1]);
    
    for (i = 0; i < 5 - 1; i++)
        if(list[i + 1] - list[i] > 1)
            return 0;

    return 1;
}

int countPairs(int pl, int db) {
    int i, c = 0;;
    int count[13] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    for (i = 0; i < 5; i++) {
        int card = playerCards[pl][i] % 13;
        count[card]++;
    }

    for(i = 0; i < 13; i++)
        if(count[i] == db)
            c++;

    return c;
}

int getYourCard(int pl) {
    if (isRoyalFlush(pl) == 1)
        return 10;
    
    if (isStraight(pl) == 1 && isFlush(pl) == 1)
        return 9;

    if (countPairs(pl, 4) == 1)
        return 8;

    if (countPairs(pl, 3) == 1 && countPairs(pl, 2) == 1)
        return 7;

    if (isFlush(pl) == 1)
        return 6;

    if (isStraight(pl) == 1)
        return 5;

    if (countPairs(pl, 3) == 1)
        return 4;

    if (countPairs(pl, 2) == 2)
        return 3;

    if (countPairs(pl, 2) == 1)
        return 2;

    if (isOneHighCard(pl) == 1)
        return 1;

    return 0;
}

void closeHandler(int dummy) {
    int i, max = MAX;
    for(i = 0; i < max; i++)
        if(sockets[i] > -1)
            close(sockets[i]);
    if(fd && fd > -1)
        shutdown(fd, SHUT_RDWR);
    printf("A szerver bezárva!\n");
    exit(1);
}

int main(int argc , char *argv[]) {
    srand(time(NULL)); //Random seed

	int fd_client, c, *new_sock, on = 1, max = MAX, i, j, id, *new_id;
	struct sockaddr_in server, client, *new_addr;

    for (i = 0; i < max; i++){
        sockets[i] = -1;
        for(j = 0; j < 5; j++)
            playerCards[i][j] = -1;

        playerRepDones[i] = 0;
        playerReps[i] = -1;
    }

    for (i = 0; i < 52; i++)
        usedCards[i] = -1;

    printf("Hány játszmát játszanak a kliensek: ");
    scanf("%d", &games);
    printf("A kliensek %d játszmát fognak játszani.\n", games);

    now = 0;

    signal(SIGINT, closeHandler);

	fd = socket(AF_INET , SOCK_STREAM , 0);
	if (fd == -1){
		printf("create fail");
        exit(1);
	}
	
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)on, sizeof on);
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)on, sizeof on);

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	
	if( bind(fd, (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("bind fail");
		return 1;
	}

	listen(fd, 10);
	
	printf("Kapcsolatok varasa...\n");
	c = sizeof(struct sockaddr_in);

	while((fd_client = accept(fd, (struct sockaddr *)&client, (socklen_t*)&c)) )
	{
        id = 0;
        while(sockets[id] != -1 && id < max)
            id++;
        if (id >= max) {
            printf("Kapcsolat elutasitva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
            close(fd_client);
            continue;
        }

        sockets[id] = fd_client;

		printf("Kapcsolat fogadva: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
		
        struct thread_args args;
		pthread_t sniffer_thread;
		new_sock = (int*)malloc(sizeof(int));
		*new_sock = fd_client;
        new_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
        *new_addr = client;
        new_id = (int*)malloc(sizeof(int));
        *new_id = id;

        args.client = new_sock;
        args.addr = new_addr;
        args.id = new_id;

		if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*)&args) < 0)
		{
			perror("could not create thread");
			return 1;
		}
	}
	
	if (fd_client < 0)
	{
		perror("accept failed");
		return 1;
	}

    close(fd);
	
	return 0;
}

void sendTo(int sock, char * msg, int len){
    char newt[len];
    int i = 0;
    do {
        newt[i] = msg[i];
    } while(msg[i++]);
    send(sock, newt, len, 0);
}

void sendCards(int id) {
    int i;
    char cardS[4];
    sendTo(sockets[id], ":getcard", 32);
    for (i = 0; i < 5; i++){
        snprintf(cardS, sizeof(cardS), "%d", playerCards[id][i]);
        send(sockets[id], cardS, 4, 0);
    }
    sendTo(sockets[id], ":sent", 32);
}

void nextRound() {
    int i, j;

    for (i = 0; i < MAX; i++){
        for(j = 0; j < 5; j++)
            playerCards[i][j] = -1;

        playerRepDones[i] = 0;
        playerReps[i] = -1;
    }

    for (i = 0; i < 52; i++)
        usedCards[i] = -1;

    giveCardsToClient();

    for (i = 0; i < MAX; i++) {
        sendCards(i);
        sendTo(sockets[i], ":startrep", 32);
    }

    now++;
}

int ifAllDone() {
    int i;
    for (i = 0; i < MAX; i++)
        if (playerReps[i] == -1 || playerRepDones[i] < playerReps[i])
            return 0;
    return 1;
}

void checkDone() {
    int i, pl;
    char name[512];
    if (ifAllDone() == 1) {
        int f1 = getYourCard(0);
        int f2 = getYourCard(1);
        if (f1 == f2) {
            if (f1 == 0) {
                printf("none\n");

                sendTo(sockets[0], ":draft", 32);
                sendTo(sockets[1], ":draft", 32);
            } else {
                printf("draft\n");

                sendTo(sockets[1], ":draft", 32);
                sendTo(sockets[1], names[f1], 32);
                pl = 0;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[1], name, 512);

                sendTo(sockets[0], ":draft", 32);
                sendTo(sockets[0], names[f1], 32);
                pl = 1;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[0], name, 512);
            }
        } else {
            if (f1 > f2) {
                printf("Az első játékos nyert '%s' > '%s'.\n", names[f1], names[f2]);
                sendTo(sockets[0], ":win", 32);
                sendTo(sockets[0], names[f1], 32);
                sendTo(sockets[0], names[f2], 32);
                pl = 1;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[0], name, 512);

                sendTo(sockets[1], ":lose", 32);
                sendTo(sockets[1], names[f2], 32);
                sendTo(sockets[1], names[f1], 32);
                pl = 0;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[1], name, 512);

            } else {
                printf("A második játékos nyert '%s' > '%s'.\n", names[f2], names[f1]);

                sendTo(sockets[1], ":win", 32);
                sendTo(sockets[1], names[f2], 32);
                sendTo(sockets[1], names[f1], 32);
                pl = 0;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[1], name, 512);

                sendTo(sockets[0], ":lose", 32);
                sendTo(sockets[0], names[f1], 32);
                sendTo(sockets[0], names[f2], 32);
                pl = 1;
                snprintf(name, sizeof(name), "%s, %s, %s, %s, %s", numberToCard(playerCards[pl][0]), numberToCard(playerCards[pl][1]), numberToCard(playerCards[pl][2]), numberToCard(playerCards[pl][3]), numberToCard(playerCards[pl][4]));
                sendTo(sockets[0], name, 512);
            }
        }

        if (now < games) {
            nextRound();
        } else {
            sendTo(sockets[0], ":end", 32);
            sendTo(sockets[1], ":end", 32);
            printf("Lejátszották az összes játékot!");

            for (i = 0; i < MAX; i++)
                if(sockets[i] > -1)
                    close(sockets[i]);

            close(fd);
            exit(1);
        }
    } 
}

void *connection_handler(void *arguments)
{
    int read_size, i, j;
    struct thread_args *args = (struct thread_args *) arguments;
	int sock = *(int*)args->client;
    int id = *(int*)args->id;
    struct sockaddr_in addr = *(args->addr);
	char client_message[32], cardS[4];

    if (id == 0) {
        sendTo(sock, ":waitforother", 32);
        printf("Fellépett egy genyo\n");
    } else {
        nextRound();
        printf("Kártyák kiosztva...\n");

        printf("Ez van az első játékosnak: %s\nÉs ez a másodiknak: %s\n", names[getYourCard(0)], names[getYourCard(1)]);
    }

	while ((read_size = recv(sock, client_message, 32, 0)) > 0) {
        if (valid(client_message, ":replace") == 0) {
            bzero(cardS, sizeof(cardS));
            recv(sock, cardS, 4, 0);
            playerReps[id] = atoi(cardS);
            playerRepDones[id] = 0;

            if (playerReps[id] > 0) {
                sendTo(sock, ":getrep", 32);
                snprintf(cardS, sizeof(cardS), "%d", playerRepDones[id]);
                sendTo(sock, cardS, 4);
                
                printf("A(z) '%d' kliens '%d' szeretne cserélni.\n", id, playerReps[id]);
            } else {
                printf("A(z) '%d' kliens nem szeretne cserélni.\n", id);
                sendTo(sock, ":done", 32);
                checkDone();
            }
        } else if (valid(client_message, ":setcard") == 0) {
            bzero(cardS, sizeof(cardS));
            recv(sock, cardS, 4, 0);
            int to = atoi(cardS);

            playerCards[id][to] = getRandomCard();
            playerRepDones[id]++;
            
            sendTo(sock, ":setnew", 32);
            bzero(cardS, sizeof(cardS));
            snprintf(cardS, sizeof(cardS), "%d", playerCards[id][to]);
            send(sock, cardS, 4, 0);

            printf("A(z) '%d' kliens le szeretné cserélni a '%d' lapját [%d/%d].\n", id, to, playerRepDones[id], playerReps[id]);

            if (playerRepDones[id] >= playerReps[id]) {
                printf("A(z) '%d' kliens lecserélte az összes lapot!\n", id);
                sendTo(sock, ":done", 32);
                checkDone();
            } else {
                sendTo(sock, ":getrep", 32);
                snprintf(cardS, sizeof(cardS), "%d", playerRepDones[id]);
                sendTo(sock, cardS, 4);
            }
        } else if (valid(client_message, ":surrender") == 0) {
            sendTo(sockets[id == 0 ? 1 : 0], ":surr", 32);
            
            for (i = 0; i < MAX; i++)
                if(sockets[i] > -1)
                    close(sockets[i]);

            close(fd);
            exit(1);
        } else {
		    write(sock, client_message, read_size); //Ismeretlen üzenet
        }
		bzero(client_message, sizeof(client_message));
	}
	
	if (read_size == 0) {
        printf("Kapcsolat bezarult: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
        sockets[id] = -1;
	} else if(read_size == -1) {
		perror("recv failed");
	}

	return 0;
}